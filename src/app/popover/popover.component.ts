import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { AccountPage } from '../account/account.page';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss']
})
export class PopoverComponent implements OnInit {

  constructor(public viewCtrl: PopoverController,public modalController: ModalController) { }

  close() {
    this.viewCtrl.dismiss();
  }

  async accountsettings() {
    const modal = await this.modalController.create({
      component: AccountPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  ngOnInit() {
  }

}
