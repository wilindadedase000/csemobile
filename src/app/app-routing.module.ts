import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'createaccount', loadChildren: './createaccount/createaccount.module#CreateaccountPageModule' },
  { path: 'faqs', loadChildren: './faqs/faqs.module#FaqsPageModule' },
  { path: 'landing', loadChildren: './landing/landing.module#LandingPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'mockactualresult', loadChildren: './mockactualresult/mockactualresult.module#MockactualresultPageModule' },
  { path: 'mockexam', loadChildren: './mockexam/mockexam.module#MockexamPageModule' },
  { path: 'mockresult', loadChildren: './mockresult/mockresult.module#MockresultPageModule' },
  { path: 'mockresultcategory', loadChildren: './mockresultcategory/mockresultcategory.module#MockresultcategoryPageModule' },
  { path: 'practexam', loadChildren: './practexam/practexam.module#PractexamPageModule' },
  { path: 'practresult', loadChildren: './practresult/practresult.module#PractresultPageModule' },
  { path: 'sentquestions', loadChildren: './sentquestions/sentquestions.module#SentquestionsPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'wall', loadChildren: './wall/wall.module#WallPageModule' },
  { path: 'popoverpage', loadChildren: './popoverpage/popoverpage.module#PopoverpagePageModule' },
  { path: 'account', loadChildren: './account/account.module#AccountPageModule' },
  // { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
