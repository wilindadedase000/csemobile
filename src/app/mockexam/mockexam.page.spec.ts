import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockexamPage } from './mockexam.page';

describe('MockexamPage', () => {
  let component: MockexamPage;
  let fixture: ComponentFixture<MockexamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockexamPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockexamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
