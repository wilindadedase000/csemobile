import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockresultPage } from './mockresult.page';

describe('MockresultPage', () => {
  let component: MockresultPage;
  let fixture: ComponentFixture<MockresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockresultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
