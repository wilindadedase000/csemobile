import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PractresultPage } from './practresult.page';

describe('PractresultPage', () => {
  let component: PractresultPage;
  let fixture: ComponentFixture<PractresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PractresultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PractresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
