import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentquestionsPage } from './sentquestions.page';

describe('SentquestionsPage', () => {
  let component: SentquestionsPage;
  let fixture: ComponentFixture<SentquestionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentquestionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentquestionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
