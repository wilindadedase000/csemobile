import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WallPage } from './wall.page';
// import { PopoverComponent } from '../popover/popover.component'
// import { PopoverpagePageModule } from '../popoverpage/popoverpage.module';

const routes: Routes = [
  {
    path: '',
    component: WallPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
    // PopoverpagePageModule
  ],
  
  declarations: [WallPage]
})
export class WallPageModule {}
