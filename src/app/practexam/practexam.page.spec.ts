import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PractexamPage } from './practexam.page';

describe('PractexamPage', () => {
  let component: PractexamPage;
  let fixture: ComponentFixture<PractexamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PractexamPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PractexamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
