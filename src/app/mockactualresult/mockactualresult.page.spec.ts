import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockactualresultPage } from './mockactualresult.page';

describe('MockactualresultPage', () => {
  let component: MockactualresultPage;
  let fixture: ComponentFixture<MockactualresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockactualresultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockactualresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
