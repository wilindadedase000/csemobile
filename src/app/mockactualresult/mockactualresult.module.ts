import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MockactualresultPage } from './mockactualresult.page';

const routes: Routes = [
  {
    path: '',
    component: MockactualresultPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MockactualresultPage]
})
export class MockactualresultPageModule {}
