import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockresultcategoryPage } from './mockresultcategory.page';

describe('MockresultcategoryPage', () => {
  let component: MockresultcategoryPage;
  let fixture: ComponentFixture<MockresultcategoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockresultcategoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockresultcategoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
