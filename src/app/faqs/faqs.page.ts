// import { Component, OnInit } from '@angular/core';
// import { AlertController } from '@ionic/angular';

// @Component({
//   selector: 'app-faqs',
//   templateUrl: './faqs.page.html',
//   styleUrls: ['./faqs.page.scss'],
// })
// export class FaqsPage implements OnInit {

//   constructor(public alertController: AlertController) { }
//   async presentAlert() {
//     const alert = await this.alertController.create({
//       header: 'What is the significance of this app?',
//       // subHeader: 'Subtitle',
//       message: 'Nulla non est eu ex congue volutpat. Vestibulum vel maximus tellus. Integer id facilisis felis, quis accumsan ipsum. Aliquam eu facilisis dui. Donec ornare elementum tortor, ut lobortis massa pretium ac. Nullam at justo sed ante efficitur pretium. Phasellus auctor, diam id congue hendrerit, orci massa dapibus erat, ac elementum augue nibh a nibh. Aliquam et accumsan tellus. Ut ut felis eros. Curabitur quis varius leo, sit amet semper erat. Donec non scelerisque turpis. Pellentesque at purus ut felis cursus tempor. Cras sed massa bibendum, mollis quam eget, gravida dolor. Maecenas blandit et eros non facilisis. Praesent sagittis turpis et suscipit accumsan. Praesent tempus metus ut est condimentum laoreet. Duis ornare, metus ac mollis consequat, lacus leo dictum nibh, at tristique nulla metus quis massa. Integer consectetur dignissim nisi, et efficitur justo volutpat non. Nunc ac lorem facilisis, porttitor ante non, hendrerit mi.',
//       buttons: ['OK']
//     });

//     await alert.present();
//   }

//   ngOnInit() {
//   }

// }

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PopoverpagePage } from '../popoverpage/popoverpage.page';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements OnInit {

  constructor(public modalController: ModalController) { }
  async presentModal() {
    const modal = await this.modalController.create({
      component: PopoverpagePage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  ngOnInit() {
  }

}
