import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  constructor(public modalController:ModalController) { }
  close() {
    this.modalController.dismiss();
  }

  ngOnInit() {
  }

}
