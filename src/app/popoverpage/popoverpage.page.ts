import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-popoverpage',
  templateUrl: './popoverpage.page.html',
  styleUrls: ['./popoverpage.page.scss'],
})
export class PopoverpagePage implements OnInit {

  constructor(public modalController: ModalController) { }
  close() {
    this.modalController.dismiss();
  }

  ngOnInit() {
  }

}
